    // Setting same height on columns with matchHeight.js plugin
    $(document).ready(function(){
      // checkbox field parent div.mktoFormCol selector - applies 'checkboxField' class
      if( $('input[type=checkbox]').length > 0 ) {
        $('input[type=checkbox]').each(function() {
          var formCol = $(this).parents('div.mktoFormCol').eq(0);
          formCol.addClass('checkboxField');
        });
      }

      if ( $('.mh').length > 0 ) {
        $('.mh').matchHeight();
      }

      var expanders = [];
      
      if ( $('.expander').length > 0 ) {
        // initial page index
        $('.expander').each(function(i) {
          var xcNode = $(this).find('.xc').eq(0),
            xControl = $(this).find('.control'),
            xch = xcNode.height();

          // $(this).attr({"data-xch" : xch});

          /*
          expanders.push({
            "node" : $(this).attr('id'),
            "height" : xch
          });
          */

          // attach behaviour to control
          xControl.on('click', function(e){
            e.preventDefault(0);
            var expander = $(this).parents('.expander').eq(0);

            if ( expander.hasClass('open') ) {
              expander.removeClass('open');
            } else {
              expander.addClass('open');
            }
          })
        });


        // re-index on window resize
        /*
        $(window).on('resize',function(e) {
          var xIndex = 0,
              xLength = expanders.length,
              xThis,
              xNode;

          do {
            xThis = expanders[xIndex];
            xNode = $('#' + xThis.node);
            xwNode = xNode.find('.window').eq(0);
            xcNode = xNode.find('.xc').eq(0);
            xch = xNode.height();
            xThis.height = xch;
            // xNode.attr({"data-xch" : xch});  

            if( xNode.hasClass('open') ) {
              xwNode.css({"max-height" : xch + 40 + 'px'});
            } else {
              xwNode.css({"max-height" : '0px'});
            }

            xIndex++;
          } while (xIndex < xLength)
        });
        */

      }
    });
    /*
    var archiveContent = document.querySelectorAll('.column-content');

    jQuery(function() {
      if (archiveContent.length > 0) {
        jQuery('.column-content').matchHeight();
      }
    });
    */
